import React from 'react'
import { Form, Button } from 'react-bootstrap'

const RegisterFormComponent = () => {
    return (
        <Form>
            <Form.Group controlId="formBasicFullName">
                <Form.Label>Your Full Name</Form.Label>
                <Form.Control type="text" placeholder="Your Name Here" />
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" />
            </Form.Group>
            <Button variant="primary" type="submit">
                Register
            </Button>
        </Form>
    )
}

export default RegisterFormComponent
