import React from 'react'
import { Col, Card } from 'react-bootstrap'

const GalleryComponent = ({ imageUrl, index }) => {
    return (
        <Col md={3} xs={5} className="mb-4" key={index}>
            <Card className="shadow" style={{
                "width": "13rem",
            }}>
                <Card.Img
                    variant="top"
                    src={imageUrl}
                    style={{
                        "height": "auto",
                        "objectFit": "cover",
                        "maxHeight": "100px",
                    }}
                >
                </Card.Img>
                <Card.Body>
                    <Card.Text>
                        Image link: <a href={imageUrl}>here</a>
                    </Card.Text>
                </Card.Body>
            </Card>
        </Col>
    )
}

export default GalleryComponent
