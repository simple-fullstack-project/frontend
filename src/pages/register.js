import React, { Component } from 'react'
import { Card, Form, Button } from 'react-bootstrap'
import AuthService from '../services/authService'

export default class register extends Component {
    constructor(props) {
        super(props)

        this.state = {
            fullName: '',
            email: '',
            password: '',
            alertMessage: ''
        }
    }

    onChangeFullName = (e) => {
        e.preventDefault();
        this.setState({ fullName: e.target.value })
    }

    onChangeEmail = (e) => {
        e.preventDefault();
        this.setState({ email: e.target.value })
    }

    onPasswordChange = (e) => {
        e.preventDefault();
        this.setState({ password: e.target.value })
    }

    onRegister = (event) => {
        event.preventDefault();
        const { fullName, email, password } = this.state
        AuthService.register({ fullName, email, password })
            .then(
                result => {
                    this.props.history.push("/dashboard");
                    window.location.reload();
                })
            .catch(error => {
                console.log('error login: ', error)
                this.setState({ alertMessage: error.message })
            })
    }
    render() {
        return (
            <Card className="shadow" style={{ width: '30rem', "margin-top": "5rem", "margin-left": 'auto', "margin-right": 'auto' }}>
                <Card.Header>
                    Ivan Simple App
                </Card.Header>
                <Card.Body>
                    <Card.Title className="text-center">
                        Register Form
                    </Card.Title>
                    <Form>
                        <Form.Group controlId="formBasicFullName">
                            <Form.Label>Your Full Name</Form.Label>
                            <Form.Control type="text" placeholder="Your Name Here" onChange={this.onChangeFullName} />
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" onChange={this.onChangeEmail} />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" onChange={this.onPasswordChange} />
                        </Form.Group>
                        <Button variant="primary" type="submit" onClick={this.onRegister}>
                            Register
                        </Button>
                    </Form>
                </Card.Body>
            </Card>
        )
    }
}
