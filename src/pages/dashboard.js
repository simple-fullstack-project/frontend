import React, { Component } from 'react'
import { Container, Row, Col, Nav, Navbar, Form, Button, FormControl, Pagination } from 'react-bootstrap'
import { GalleryComponent } from '../components'
import { API_URL } from '../utils/constant'
import axios from 'axios'
import authHeader from '../services/authHeader'
import authService from '../services/authService'

export default class dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activePage: 1,
            itemsPage: [],
            imageUrl: [],
            showImage: [],
            tags: '',
            fullName: ''
        };
    }
    componentDidMount() {
        if (!authHeader()) {
            this.props.history.push("/login");
            window.location.reload();
            return
        }

        const { fullName } = JSON.parse(localStorage.getItem('user'))
        this.setState({ fullName })
        axios.get(`${API_URL}/flickr-feed`, { headers: authHeader() })
            .then((res) => {
                const imageUrl = res.data.result;
                this.setState({ imageUrl });
                this.setState({ showImage: imageUrl.slice((this.state.activePage - 1) * 4, this.state.activePage * 4) })
            })
            .catch((error) => {
                console.log("Error yaa ", error);
            });

        this.setPagination();

    }
    paginationChange = (e) => {
        const page = parseInt(e.target.innerHTML)
        const imageArray = this.state.imageUrl
        this.setState({ showImage: imageArray.slice((page - 1) * 4, page * 4) })
        const paging = [];
        for (let number = 1; number <= 5; number++) {
            paging.push(
                <Pagination.Item key={number} active={number === page} onClick={this.paginationChange}>
                    {number}
                </Pagination.Item>,
            );
        }
        this.setState({ itemsPage: paging })
    }

    setPagination = () => {
        const paging = [];
        for (let number = 1; number <= 5; number++) {
            paging.push(
                <Pagination.Item key={number} active={number === 1} onClick={this.paginationChange}>
                    {number}
                </Pagination.Item>,
            );
        }
        this.setState({ itemsPage: paging })
    }

    formChange = (event) => {
        event.preventDefault();
        this.setState({ tags: event.target.value })
    }
    doSearchTags = (event) => {
        event.preventDefault();
        if (this.state.tags === '') {
            return
        }
        axios
            .get(`${API_URL}/flickr-search/${this.state.tags}`, { headers: authHeader() })
            .then((res) => {
                const imageUrl = res.data.result;
                this.setState({ imageUrl });
                this.setState({ showImage: imageUrl.slice((this.state.activePage - 1) * 4, this.state.activePage * 4) })
            })
            .catch((error) => {
                this.props.history.push("/login");
                window.location.reload();
            });
        this.setPagination();
    }
    doLogout = () => {
        authService.logout()
        this.props.history.push("/login");
        window.location.reload();
    }

    render() {
        const { showImage } = this.state;
        return (
            <div className="mt-3">
                <Container fluid>
                    <Navbar bg="dark" variant="dark" expand="lg">
                        <Navbar.Brand href="#home">Ivan Simple App</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="mr-auto">
                            </Nav>
                            <Navbar.Text>
                                Welcome! {`${this.state.fullName}`}
                            </Navbar.Text>
                            <Button variant="link" onClick={this.doLogout}>
                                Logout
                            </Button>
                        </Navbar.Collapse>
                    </Navbar>
                    <hr />
                    <Form>
                        <Form.Row>
                            <Col sm="11">
                                <FormControl type="text" placeholder="Search Image by Tags" onChange={this.formChange} onSubmit={event => { event.preventDefault() }} />
                            </Col>
                            <Col>
                                <Button variant="outline-success" onClick={this.doSearchTags}>Search</Button>
                            </Col>
                        </Form.Row>
                    </Form>
                    <hr />
                    <Row className="overflow-auto">
                        {showImage &&
                            showImage.map((image, index) => (
                                <GalleryComponent imageUrl={image} index={index} />
                            ))
                        }
                    </Row>
                    <hr />
                    <div style={{ display: "block", width: "25%", marginLeft: "auto", marginRight: "auto" }}>
                        <Pagination>{this.state.itemsPage}</Pagination>
                    </div>
                </Container>
            </div>
        )
    }
}
