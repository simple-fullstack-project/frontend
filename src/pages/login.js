import React, { Component } from 'react'
import { Card, Form, Button, Alert } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import AuthService from '../services/authService'

export default class login extends Component {
    constructor(props) {
        super(props)

        this.state = {
            email: '',
            password: '',
            alertMessage: ''
        }
    }

    onChangeEmail = (e) => {
        e.preventDefault();
        this.setState({ email: e.target.value })
    }

    onPasswordChange = (e) => {
        e.preventDefault();
        this.setState({ password: e.target.value })
    }

    onLogin = (event) => {
        event.preventDefault();
        const { email, password } = this.state
        AuthService.login({ email, password })
            .then(
                result => {
                    this.props.history.push("/dashboard");
                    window.location.reload();
                })
            .catch(error => {
                console.log('error login: ', error)
                this.setState({ alertMessage: error.message })
            }
            )
    }

    render() {
        return (
            <Card className="shadow" style={{ width: '30rem', "marginTop": "5rem", "marginLeft": 'auto', "marginRight": 'auto' }}>
                <Card.Header>
                    Ivan Simple App
                </Card.Header>
                <Card.Body>
                    <Card.Title className="text-center">
                        Login Form
                    </Card.Title>
                    <Card.Text style={{ textAlign: "center" }}>
                        To use my simple app, login first please.
                    </Card.Text>
                    <Form>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" onChange={this.onChangeEmail} onSubmit={event => { event.preventDefault(); }} />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" onChange={this.onPasswordChange} onSubmit={event => { event.preventDefault(); }} />
                        </Form.Group>
                        {this.state.alertMessage !== '' &&
                            <Alert show={true} variant="danger">
                                {this.state.alertMessage}
                            </Alert>
                        }
                        <Button variant="primary" type="submit" onClick={this.onLogin}>
                            Login
                        </Button>
                        <Button variant="link" as={Link} to="/register">
                            Register
                        </Button>
                        <Form.Text className="text-muted">
                            If you never register before please register first.
                        </Form.Text>
                    </Form>
                </Card.Body>
            </Card>
        )
    }
}
