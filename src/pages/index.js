import login from './login'
import register from './register'
import dashboard from './dashboard'

export { login, register, dashboard }