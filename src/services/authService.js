import axios from 'axios'
import { API_URL } from '../utils/constant'

class AuthService {
    async login(data) {
        let returnData;
        await axios.post(`${API_URL}/login`, data)
            .then(response => {
                if (response.data.token) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }
                returnData = response.data;
            })
            .catch(error => {
                const data = error.response.data.message
                throw new Error(data)
            })
        return returnData
    }
    async register(data) {
        let returnData;
        await axios.post(`${API_URL}/register`, data)
            .then(response => {
                if (response.data.token) {
                    localStorage.setItem("user", JSON.stringify(response.data));
                }
                returnData = response.data;
            })
            .catch(error => {
                const data = error.response.data.message
                throw new Error(data)
            })
        return returnData

    }
    logout() {
        localStorage.removeItem("user");
    }
}

export default new AuthService();