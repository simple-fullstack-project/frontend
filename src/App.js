import React, { Component } from 'react'
import {
  BrowserRouter,
  Switch,
  Route,
} from "react-router-dom";
import { login, register, dashboard } from './pages'

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <main>
          <Switch>
            <Route path="/" component={login} exact />
            <Route path="/login" component={login} exact />
            <Route path="/register" component={register} exact />
            <Route path="/dashboard" component={dashboard} exact />
          </Switch>
        </main>
      </BrowserRouter>
    )
  }
}
